# the inclusion of the tests module is not meant to offer best practices for
# testing in general, but rather to support the `find_packages` example in
# setup.py that excludes installing the "tests" package
import filecmp
import os
import tempfile
import unittest

from sample.simple import (
    wrapper,
    make_memacs_argument,
    MEMACS_ARGUMENT,
    skip_first_two_lines,
    return_input_file_name,
)

from cli_test_helpers import ArgvContext


class test_cli_command_unittest(unittest.TestCase):
    """
    Is the correct code called when invoked via the CLI?
    """

    def test_check_main(self):
        """Check if wrapper function is available"""
        with ArgvContext("foobar", "baz"), self.assertRaises(SystemExit):
            wrapper()
        """Fixme. this test leaves tmp file"""

    def test_exit_unexpected_extra_argument(self):
        """test exit with unexpected extra arguments """
        with ArgvContext("taskuma2org", "foo", "bar"), self.assertRaises(
            SystemExit
        ) as cm:
            wrapper()
        self.assertNotEqual(cm.exception.code, 0)
        """Fixme. this test leaves tmp file"""


class test_skip_two_line(unittest.TestCase):
    def test_skip_two_line_function(self):
        """compare the file with the top two lines removed against the preset file"""
        fp = open("tests/data/examplewithheader.csv")
        result = skip_first_two_lines(fp, skip=True)
        self.assertTrue(
            filecmp.cmp(result, "tests/data/examplewithout.csv", shallow=False)
        )
        os.remove(result)

    def test_return_input_file_name_with_skip(self):
        """ test if return different file name"""
        fp = open("tests/data/examplewithout.csv")
        result = return_input_file_name(fp, skip=True)
        self.assertNotEqual(result, "tests/example1.csv")
        os.remove(result)

    def test_return_input_file_name_without_skip(self):
        """ test if return different file name"""
        fp = open("tests/data/examplewithout.csv")
        result = return_input_file_name(fp, skip=False)
        self.assertNotEqual(result, "tests/data/examplewithout.csv")


class test_construct_command_string(unittest.TestCase):
    """ test constructing intended shell command string"""

    def test_memacs_argument_strings_both_infile_and_outfile(self):
        """test if constructs shell command specifying both infile and outfile"""
        ret = make_memacs_argument("infile", "outfile")
        self.assertEqual(ret, MEMACS_ARGUMENT + " -f " + "infile" + " -o " + "outfile")

    def test_memacs_argument_strings_infile_only(self):
        """test if constructs shell command specifying infile only"""
        ret = make_memacs_argument("infile")
        self.assertEqual(ret, MEMACS_ARGUMENT + " -f " + "infile")


class functional_tests_without_output_file(unittest.TestCase):
    """functional tests without specifying output file"""

    def test_no_skip_command_without_output_file(self):
        with ArgvContext(
            "taskuma2org",
            "--no-skip",
            "tests/data/examplewithout.csv",
        ), self.assertRaises(SystemExit) as cm:
            wrapper()
        self.assertEqual(cm.exception.code, 0)

    def test_default_no_skip_command_without_output_file(self):
        with ArgvContext(
            "taskuma2org",
            "tests/data/examplewithout.csv",
        ), self.assertRaises(SystemExit) as cm:
            wrapper()
        self.assertEqual(cm.exception.code, 0)

    def test_no_skip_command_without_output_file_which_fail(self):
        """ command should fail because infile has header lines"""
        with ArgvContext(
            "taskuma2org",
            "--no-skip",
            "tests/data/examplewithheader.csv",
        ), self.assertRaises(SystemExit) as cm:
            wrapper()
        self.assertNotEqual(cm.exception.code, 0)  # command should fail

    def test_skip_command_without_output_file(self):
        """normal case, command should return 0"""
        with ArgvContext(
            "taskuma2org",
            "--skip",
            "tests/data/examplewithheader.csv",
        ), self.assertRaises(SystemExit) as cm:
            wrapper()
        self.assertEqual(cm.exception.code, 0)


class functional_tests_with_output_file(unittest.TestCase):
    """functional tests specifying output file"""

    def setUp(self):
        with tempfile.NamedTemporaryFile(delete=False) as fp:
            fp.close

        self.tmpfilename = fp.name

    def tearDown(self):
        os.remove(self.tmpfilename)

    def test_no_skip_command_with_output_file(self):
        with ArgvContext(
            "taskuma2org",
            "--no-skip",
            "tests/data/examplewithout.csv",
            "--output",
            self.tmpfilename,
        ), self.assertRaises(SystemExit) as cm:
            wrapper()
        self.assertEqual(cm.exception.code, 0)

    def test_default_no_skip_command_with_output_file(self):
        with ArgvContext(
            "taskuma2org",
            "tests/data/examplewithout.csv",
            "--output",
            self.tmpfilename,
        ), self.assertRaises(SystemExit) as cm:
            wrapper()
        self.assertEqual(cm.exception.code, 0)

    def test_no_skip_command_with_output_file_which_fail(self):
        """ command should fail because infile has header lines"""
        with ArgvContext(
            "taskuma2org",
            "--no-skip",
            "tests/data/examplewithheader.csv",
            "--output",
            self.tmpfilename,
        ), self.assertRaises(SystemExit) as cm:
            wrapper()
        self.assertNotEqual(cm.exception.code, 0)  # command should fail

    def test_skip_command_with_output_file(self):
        """normal case, command should return 0"""
        with ArgvContext(
            "taskuma2org",
            "--skip",
            "tests/data/examplewithheader.csv",
            "--output",
            self.tmpfilename,
        ), self.assertRaises(SystemExit) as cm:
            wrapper()
        self.assertEqual(cm.exception.code, 0)


class functional_tests_with_pipe_without_output_file(unittest.TestCase):
    """functional tests without specifying output file"""

    def test_no_skip_command_with_pipe_without_output_file(self):
        exit_status = os.system("cat tests/data/example1.csv | taskuma2org -")
        self.assertEqual(exit_status, 0)
