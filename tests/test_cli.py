"""
Tests for command line interface (CLI)
"""
import os


def test_entrypoint():
    """
    Is entrypoint script installed? (setup.py)
    """
    exit_status = os.system("taskuma2org tests/data/example1.csv")
    assert exit_status == 0
