#!/usr/bin/env python

"""Tests for `memacstaskuma` package."""


import os
import unittest

from memacs.csv import Csv


class TestCsv(unittest.TestCase):
    def test_example1(self):
        example1 = os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "data", "example1.csv"
        )

        argv = []
        argv.append("-f")
        argv.append(example1)
        argv.append("--timestamp-field")
        argv.append("start")
        argv.append("--fieldnames")
        argv.append(
            "task, est, tasukumaactual, start, end, rate, taskumaproj, \
            section, map, memo, check, taskumatag, dummy"
        )
        argv.append("--delimiter")
        argv.append(",")
        argv.append("--timestamp-format")
        argv.append("%Y/%m/%d %H:%M")
        argv.append("--output-format")
        argv.append("{task}")
        argv.append("--properties")
        argv.append("tasukumaactual,taskumaproj,section,memo,taskumatag")

        memacs = Csv(argv=argv)
        data = memacs.test_get_entries()

        self.assertEqual(data[0], "** <2018-09-26 Wed 05:36> check this")
        self.assertEqual(data[1], "   :PROPERTIES:")
        self.assertEqual(data[2], "   :TASUKUMAACTUAL: 31")
        self.assertEqual(data[3], "   :TASKUMAPROJ:    020[study]")
        self.assertEqual(data[4], "   :SECTION:        4 - 8")
        self.assertEqual(
            data[5],
            "   :MEMO:           2018/09/26 06:08 Took time \
Looked for time table",
        )
        self.assertEqual(data[6], "   :TASKUMATAG:     ")
        self.assertEqual(
            data[7],
            "   :ID:             \
180f303489891028bc02104048946596b738a9ee",
        )
        self.assertEqual(data[8], "   :END:")


if __name__ == "__main__":
    unittest.main()
